This application contains a text box for the user to enter a comma separated
list of their lunch items.
When the 'check if too much button' is pressed, a message is displayed based on
the number of items entered. An empty or white space only item in the list is
not counted towards the total lunch item count (i.e. foo,,,bar is considered
to be 2 lunch items. The entry ,,,, is treated as an empty list)
