(function () {
'use strict';

angular.module('LunchChecker', []).controller('LunchCheckerController', ['$scope', function($scope){
  //scope variables
  $scope.lunchOrderInput = "";
  $scope.lunchOrderStatus = "";
  $scope.statusCss = "";

  //called when the user clicks the lunch check button
  $scope.determineAndDisplayLunchOrderStatusMessage = function(){
    //trim the lunch order text
    $scope.lunchOrderInput = $scope.lunchOrderInput.trim();

    //split the string using comma as a delimiter
    var lunchItemsArray = $scope.lunchOrderInput.split(',');
    var numberOfValidLunchItems = 0;

    //loop over each item in the lunch array to determine how many actual lunch items
    for (var i = 0; i < lunchItemsArray.length; i++) {
      if(lunchItemsArray[i].trim().length != 0){
        numberOfValidLunchItems++;
      }
    }
    ////If the textbox is empty the message "Please enter data first" should show up.
    if(numberOfValidLunchItems == 0){
      $scope.statusCss = "red-output";
      $scope.lunchOrderStatus = "Please enter data first";
    }
    //If the number of items in the textbox is less than or equal to 3 (e.g., 1, 2, or 3), a message should show up under to the textbox saying "Enjoy!"
    else if(numberOfValidLunchItems <=3){
      $scope.statusCss = "green-output";
      $scope.lunchOrderStatus = "Enjoy!";
    }
    // If the number of items is greater than 3 (4, 5, and above), the message "Too much!" should show up under the textbox.
    else {
      $scope.statusCss = "green-output";
      $scope.lunchOrderStatus = "Too much!";
    }
  };
}]);

})();
